# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#
import sqlite3
import os
import logging

class Database:
    def __init__(self, path = None):
        if path is None:
            path = 'odometer.db'

        if not os.path.exists(path):
            self._conn = self._connect(path)
            self.create_database()
        else:
            self._conn = self._connect(path)

    def _connect(self, path):
        conn = sqlite3.connect(path, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
        conn.execute('PRAGMA foreign_keys = ON');
        conn.commit();
        conn.row_factory = sqlite3.Row
        return conn

    def create_database(self):
        conn = self._conn

        c = conn.cursor()
        # List of known users
        c.execute('CREATE TABLE users (' +
            'user_id integer primary key, ' +
            'name string NOT NULL' +
            ')'
        )
        # List of known devices
        c.execute('CREATE TABLE devices (' +
            'device_id integer primary key, ' +
            'device_guid string, ' +
            'description string' +
            ')'
        );
        # Users currently using devices
        c.execute('CREATE TABLE device_user (' +
            'device_id integer, ' +
            'user_id integer UNIQUE, ' +
            'session_id integer UNIQUE, ' +
            'PRIMARY KEY(device_id), ' +
            'FOREIGN KEY(device_id) REFERENCES devices(device_id) ON DELETE CASCADE, ' +
            'FOREIGN KEY(user_id) REFERENCES users(user_id) ON DELETE CASCADE, ' +
            'FOREIGN KEY(session_id) REFERENCES sessions(session_id) ON DELETE SET NULL' +
            ')'
        );
        # Training session values history
        c.execute('CREATE TABLE history (' +
            'history_id integer primary key, ' +
            'timestamp float, ' +
            'rotations integer, ' +
            'distance float, ' +
            'speed float, ' +
            'calories float, ' +
            'heart_rate float, ' +
            'session_id integer NOT NULL, ' +
            'FOREIGN KEY(session_id) REFERENCES sessions(session_id) ON DELETE CASCADE' +
            ')'
        )
        # Training sessions
        c.execute('CREATE TABLE sessions (' +
            'session_id integer primary key, ' +
            'start_time float NOT NULL, ' +
            'end_time float, ' +
            'device_id integer NOT NULL, ' +
            'user_id integer, ' +
            'FOREIGN KEY(user_id) REFERENCES users(user_id) ON DELETE CASCADE, ' +
            'FOREIGN KEY(device_id) REFERENCES devices(device_id) ON DELETE CASCADE' +
            ')'
        )
        conn.commit()

        return conn

    def close(self):
        self._conn.close()

    def start_session(self, start_time, device_id):
        """
        Starts a new training session on the given device.
        The user id will be retrieved from device_user, if any.
        :param start_time: timestamp of the session start
        :param device_id: id of the device on which the session starts
        """
        logging.debug('Session starting for device %i', device_id)

        # check what user is currently logged in to device
        c = self._conn.cursor()
        result = c.execute('SELECT user_id FROM device_user WHERE device_id=?', (device_id,))
        r = result.fetchone()
        if r:
            user_id = r['user_id']
        else:
            user_id = None

        c.execute('INSERT INTO sessions (start_time, user_id, device_id) VALUES (?, ?, ?)', (start_time, user_id, device_id))
        session_id = self._get_last_insert_id()
        c.execute('INSERT OR REPLACE INTO device_user (device_id, user_id, session_id) VALUES (?, ?, ?)',
            (device_id, user_id, session_id)
        )
        self._conn.commit()

        c.close()

        logging.debug('Session %i started for device %i', session_id, device_id)

        # update session id in current data
        #self.set_current_device_user_session(device_id, user_id, session_id)

        return session_id

    def end_session(self, session_id, end_time):
        c = self._conn.cursor()
        logging.debug('Session ending %i', session_id)
        c.execute('UPDATE sessions SET end_time=? WHERE session_id=?', (end_time, session_id))
        c.execute('UPDATE device_user SET session_id=NULL WHERE session_id=?', (session_id,))
        self._conn.commit()
        logging.debug('Session ended %i', session_id)
        return session_id

    def _close_unclosed_sessions(self, threshold_timestamp):
        """
        Closes unclosed sessions which last tick happened before the given threshold time
        :param threshold_timestamp: timestamp after which to close idle sessions
        """

        # Find unclosed sessions
        c = self._conn.cursor()
        results = c.execute('SELECT s.session_id, MAX(h.timestamp) "max_timestamp" FROM sessions s, history h WHERE s.session_id=h.session_id AND end_time IS NULL GROUP BY s.session_id')
        for result in results:
            # Everything from before the threshold gets closed
            if result['max_timestamp'] < threshold_timestamp:
                self.end_session(result['session_id'], result['max_timestamp'])

    def _delete_short_sessions(self, minimum_session_duration):
        """
        Deletes sessions which duration is less than the given minimum session duration
        :param minimum_session_duration: minimum time in seconds for a session to be valid,
        all sessions shorter than this will get deleted
        """
        c = self._conn.cursor()
        c.executemany('DELETE FROM sessions WHERE end_time IS NOT NULL AND (end_time - start_time) < ?', [(minimum_session_duration,)])
        self._conn.commit()

    def cleanup_sessions(self, threshold_timestamp, minimum_session_duration):
        """
        Cleans up too short sessions and closes past unclosed sessions.
        :param threshold_timestamp: timestamp after which to close idle sessions
        :param minimum_session_duration: minimum time in seconds for a session to be valid,
        all sessions shorter than this will get deleted
        """

        self._close_unclosed_sessions(threshold_timestamp)
        self._delete_short_sessions(minimum_session_duration)

    def _format_device(self, result):
        return {
            'device_id': result['device_id'],
            'device_guid': result['device_guid'],
            'description': result['description'],
        }

    def create_device(self, device_guid, description = ''):
        c = self._conn.cursor()
        c.executemany('INSERT INTO devices (device_guid, description) VALUES (?, ?)', [(device_guid, description)])
        self._conn.commit()

        return self._format_device({
            'device_id': self._get_last_insert_id(),
            'device_guid': device_guid,
            'description': description
        })

    def get_device_by_id(self, device_id):
        c = self._conn.cursor()
        r = c.execute('SELECT device_id, device_guid, description FROM devices WHERE device_id=?', [(device_id)])
        result = r.fetchone()

        if result is None:
            return None

        return self._format_device(result)

    def get_device_by_guid(self, device_guid):
        c = self._conn.cursor()
        r = c.execute('SELECT device_id, device_guid, description FROM devices WHERE device_guid=?', [(device_guid)])
        result = r.fetchone()

        if result is None:
            return None

        return self._format_device(result)

    def get_device(self, device_guid):
        return self.get_device_by_guid(device_guid)

    def get_devices(self):
        c = self._conn.cursor()
        results = c.execute('SELECT device_id, device_guid, description FROM devices')

        devices = []
        for result in results:
            devices.append(self._format_device(result))

        return devices

    def _format_user(self, result):
        return {
            'user_id': result['user_id'],
            'name': result['name'],
        }

    def get_or_create_user(self, name, create = True):
        c = self._conn.cursor()
        r = c.execute('SELECT user_id, name FROM users WHERE name=?', [(name)])
        result = r.fetchone()
        if create and result is None:
            c = self._conn.cursor()
            c.executemany('INSERT INTO users (name) VALUES (?)', [(name,)])
            self._conn.commit()

            return self._format_user({
                'user_id': self._get_last_insert_id(),
                'name': name
            })

        if result is None:
            return None

        return self._format_user(result)

    def get_user(self, user_id):
        c = self._conn.cursor()
        r = c.execute('SELECT user_id, name FROM users WHERE user_id=?', [(user_id)])
        result = r.fetchone()
        if result is None:
            return None

        return self._format_user(result)

    def _format_session_stat(self, result):
        return {
            'session_id': result['session_id'],
            'start_time': result['start_time'],
            'end_time': result['end_time'],
            'user_id': result['user_id'],
            'device_id': result['device_id'],
            'average_speed': result['average_speed'],
            'max_speed': result['max_speed'],
            'distance': result['distance'],
            'max_heart_rate': result['max_heart_rate'],
        }

    def get_sessions_stats(self, user_id = None, device_id = None):
        """
        Returns all odometer sessions of the given user id
        :param user_id: optional user_id
        :return: list of odometer session objects
        :rtype: list
        """
        c = self._conn.cursor()

        where = []
        params = []
        if user_id is not None:
            where .append('user_id=?')
            params.append(user_id)

        if device_id is not None:
            where.append('device_id=?')
            params.append(device_id)

        if where:
            where = ' AND ' + ' AND '.join(where)
        else:
            where = ''

        sql = (
            'SELECT s.session_id "session_id", s.start_time, s.end_time, s.device_id, s.user_id, ' +
            'AVG(speed) "average_speed", MAX(speed) "max_speed", MAX(distance) "distance", ' +
            'MAX(heart_rate) "max_heart_rate" ' +
            'FROM sessions s, history h ' +
            'WHERE s.session_id=h.session_id %s AND h.speed > 0 ' +
            'GROUP BY h.session_id ' +
            'ORDER BY s.start_time DESC'
        ) % where

        if params:
            results = c.execute(sql, tuple(params))
        else:
            results = c.execute(sql)

        sessions = []
        for result in results:
            sessions.append(self._format_session_stat(result))

        return sessions

    def session_exists(self, session_id):
        """
        Returns whether the given odometer session id exists
        :param session_id: odometer session id
        :return: True if exists, False otherwise
        :rtype: bool
        """
        c = self._conn.cursor()
        results = c.execute('SELECT session_id FROM sessions WHERE session_id=?', (session_id, ))
        r = results.fetchone()
        if r is not None:
            return True
        return False

    def _format_session(self, result):
        return {
            'session_id': result['session_id'],
            'start_time': result['start_time'],
            'end_time': result['end_time'],
            'user_id': result['user_id'],
            'device_id': result['device_id'],
        }

    def get_session(self, session_id):
        """
        Returns the odometer session from the given id
        :param session_id: odometer session id
        :return: odometer session object without stats
        :rtype: dict
        """
        c = self._conn.cursor()
        # FIXME: party duplicate with get_sessions
        results = c.execute(
            'SELECT s.session_id "session_id", s.start_time, s.end_time, s.device_id, s.user_id ' +
            'FROM sessions s ' +
            'WHERE s.session_id=? ' +
            'ORDER BY s.start_time DESC',
            (session_id, )
        )
        r = results.fetchone()
        if r is not None:
            return self._format_session(r)
        return None

    def get_session_stats(self, session_id):
        """
        Returns the odometer session with stats from the given id
        :param session_id: odometer session id
        :return: odometer session object
        :rtype: dict
        """
        c = self._conn.cursor()
        # FIXME: party duplicate with get_sessions
        results = c.execute(
            'SELECT s.session_id "session_id", s.start_time, s.end_time, s.device_id, s.user_id, ' +
            'AVG(speed) "average_speed", MAX(speed) "max_speed", MAX(distance) "distance", ' +
            'MAX(heart_rate) "max_heart_rate" ' +
            'FROM sessions s ' +
            'LEFT JOIN history h ON s.session_id=h.session_id '
            'WHERE s.session_id=? ' +
            'GROUP BY h.session_id ' +
            'ORDER BY s.start_time DESC',
            (session_id, )
        )
        r = results.fetchone()
        if r is not None:
            return self._format_session_stat(r)
        return None

    def delete_session(self, session_id):
        """
        Deletes the odometer session with the given id
        :param session_id: odometer session id
        """
        c = self._conn.cursor()
        c.execute('DELETE FROM sessions WHERE session_id=? ', (session_id,))
        self._conn.commit()

    def get_user_session_id(self, user_id):
        """
        Returns the current session id of the given user
        :param user_id: user id
        :return: current session id
        :rtype: int
        """
        c = self._conn.cursor()
        results = c.execute('SELECT session_id FROM device_user WHERE user_id=?', (user_id, ))

        r = results.fetchone()
        if r:
            return r['session_id']

        return None

    def _format_session_history(self, result):
        return {
            'history_id': result['history_id'],
            'timestamp': result['timestamp'],
            'rotations': result['rotations'],
            'distance': result['distance'],
            'speed': result['speed'],
            'heart_rate': result['heart_rate'],
        }

    def get_session_history(self, session_id):
        history = []

        c = self._conn.cursor()
        results = c.execute('SELECT * FROM history WHERE session_id=? ', (session_id,))
        for result in results:
            history.append(self._format_session_history(result))
        return history

    def get_users(self):
        users = []

        c = self._conn.cursor()
        results = c.execute('SELECT user_id, name FROM users')
        for result in results:
            users.append(self._format_user(result))
        return users

    def _get_last_insert_id(self):
        c = self._conn.cursor()
        r = c.execute('SELECT last_insert_rowid()')
        return r.fetchone()[0]

    def insert_history(self, session_id, timestamp, rotations, distance, speed, calories = 0, heart_rate = 0):
        c = self._conn.cursor()

        c.executemany('INSERT INTO history (session_id, timestamp, rotations, distance, speed, calories, heart_rate) VALUES (?, ?, ?, ?, ?, ?, ?)',
            [(session_id, timestamp, rotations, distance, speed, calories, heart_rate)]
        )

        # TODO: batch commits, once per minute ?
        self._conn.commit()

        return self._get_last_insert_id()

    def get_current_device_user_session_by_device(self, device_id):
        """
        Returns the current session for the given device
        :param device_id: device id
        :return: object with device_id, user_id and session_id
        """
        c = self._conn.cursor()
        result = c.execute('SELECT device_id, user_id, session_id FROM device_user WHERE device_id=?', (device_id,))

        r = result.fetchone()
        if r:
            logging.debug('Retrieved current device user session: device_id=%s user_id=%s session_id=%s', r['device_id'], r['user_id'], r['session_id'])
        else:
            logging.debug('Retrieved current device user session: device_id=%s none' % device_id)

        return dict(r)

    def get_current_device_user_session_by_user(self, user_id):
        """
        Returns the current session for the given user
        :param device_id: device id
        :param user_id: user id
        :return: object with device_id, user_id and session_id
        """
        c = self._conn.cursor()
        result = c.execute('SELECT device_id, user_id, session_id FROM device_user WHERE user_id=?', (user_id,))

        r = result.fetchone()
        if r:
            logging.debug('Retrieved current device user session: device_id=%s user_id=%s session_id=%s', r['device_id'], r['user_id'], r['session_id'])
        else:
            logging.debug('Retrieved current device user session: user_id=%i none' % user_id)

        return dict(r)

    def get_current_device_user_session_by_session(self, session_id):
        """
        Returns the current device and user for the given session
        :param device_id: device id
        :param session_id: session id
        :return: object with device_id, user_id and session_id
        """
        c = self._conn.cursor()
        result = c.execute('SELECT device_id, user_id, session_id FROM device_user WHERE session_id=?', (session_id,))

        r = result.fetchone()
        if r:
            logging.debug('Retrieved current device user session: device_id=%s user_id=%s session_id=%s', r['device_id'], r['user_id'], r['session_id'])
        else:
            logging.debug('Retrieved current device user session: session_id=%i none' % session_id)

        return dict(r)

    def set_current_device_user_session(self, device_id, user_id, session_id = None):
        """
        Sets the current session and user on the given device
        :param device_id: device id
        :param user_id: user id or None if no user there
        :param session_id: session id or None if no current session exists
        :return: object with user_id and session_id
        """
        c = self._conn.cursor()

        logging.debug('Setting current device user session: device_id=%s user_id=%s session_id=%s', device_id, user_id, session_id)
        c.execute('INSERT OR REPLACE INTO device_user (device_id, user_id, session_id) VALUES (?, ?, ?)',
            (device_id, user_id, session_id)
        )

        self._conn.commit()

    def set_session_start(self, session_id, new_timestamp):
        """
        Sets the start time of a session
        :param session_id: session id or None if no current session exists
        :param new_timestamp: timestamp of session start
        :return: True for success, False for failure when session was not found
        :rtype: bool
        """

        c = self._conn.cursor()

        result = c.execute('SELECT start_time FROM sessions WHERE session_id=? AND end_time IS NOT NULL', (session_id,))
        r = result.fetchone()
        if r:
            old_timestamp = r['start_time']
        else:
            return False

        timestamp_diff = new_timestamp - old_timestamp

        logging.debug('Set session start session_id=%s old_timestamp=%i new_timestamp=%i', session_id, old_timestamp, new_timestamp)
        c.execute('UPDATE sessions SET start_time=start_time+?, end_time=end_time+? WHERE session_id=? ', (timestamp_diff, timestamp_diff, session_id))

        if c.rowcount == 0:
            return False

        c.execute('UPDATE history SET timestamp=timestamp+? WHERE session_id=? ', (timestamp_diff, session_id))

        self._conn.commit()

        return True

