# Odometer Server

The Odometer Server stores the history of cycling sessions and exposes near real-time speed values to a web front-end.

It is written in Python with Flask and uses [Socket.io](https://socket.io/) to receive values from an [Odometer Service](https://gitlab.com/PVince81/odometer).
It is designed to be used with the [Odometer Web](https://gitlab.com/PVince81/odometer-web) front-end to monitor the current cycling session and display statistics from past sessions.

## Features

- Receives near real-time speed values from an [Odometer Service](https://gitlab.com/PVince81/odometer) through [Socket.io](https://socket.io/)
- Provides near real-time values to a [Odometer Web](https://gitlab.com/PVince81/odometer-web) front-end
- Stores and provides data from past cycling sessions to be visualized in the [Odometer Web](https://gitlab.com/PVince81/odometer-web) front-end
- Server-side charts for past sessions using matplotlib

## Requirements

- Python 3.5+
- An instance of [Odometer Service](https://gitlab.com/PVince81/odometer) to receive speed values from, possibly running on the same hardware
- Optionally [Odometer Web](https://gitlab.com/PVince81/odometer-web) frontend to view cycling sessions in a web browser

## Installation

```
% virtualenv ~/.virtualenv/odometer-server
% source ~/.virtualenv/odometer-server/bin/activate
% pip3 install -r requirements.txt
```

## Configuration

```
% mkdir data
% cp config.ini.sample config.ini
```

Edit the file accordingly.

## Running

```
% source ~/.virtualenv/odometer-server/bin/activate
% ./server.py
```

