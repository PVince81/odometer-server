#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#
import os

class ChartManager:
    def __init__(self, thumbnail_path = None):
        self._thumbnail_path = thumbnail_path

    def _get_chart_file_name(self, id):
        return os.path.join(self._thumbnail_path, 'session_chart_%i.png' % id)

    def make_chart(self, id, db):
        filename = self._get_chart_file_name(id)

        # if not cached, generate
        if not os.path.isfile(filename):
            values = db.get_session_history(id)
            self.generate_bar_chart(values, filename)

        return filename

    def delete_chart(self, id):
        filename = self._get_chart_file_name(id)

        if os.path.isfile(filename):
            os.remove(filename)

    def generate_bar_chart(self, values, buf, max_time_seconds = 3600.0):
        import matplotlib
        matplotlib.use('Agg')
        import matplotlib.pyplot as plt

        # strip leading and trailing zeroes
        values = self._strip_values(values)

        # TODO: use DPI as input for size
        fig = plt.figure(figsize=(3, 0.5))

        x_values = [value['timestamp'] for value in values]
        y_values = [value['speed'] for value in values]

        ax = plt.gca()
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        ax.set_ylim(0.0, 80.0)
        ax.set_xlim(values[0]['timestamp'], values[0]['timestamp'] + max_time_seconds)
        #ax.axis('off')

        plt.plot(x_values, y_values)
        plt.savefig(buf, format='png', bbox_inches='tight', transparent="True", pad_inches=0)


    def _strip_values(self, values, min_threshold = 10.0):
        start_index = 0
        end_index = len(values)

        while (start_index < len(values) and values[start_index]['speed'] <= min_threshold):
            start_index += 1

        end_index = len(values) - 1
        while (end_index > 0 and values[end_index]['speed'] <= min_threshold):
            end_index -= 1

        if end_index < start_index:
            end_index = start_index

        return values[start_index:end_index]

