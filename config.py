#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#

import configparser
import os

CONFIG_COMMON = "Common"

def load_config(config):
    config_file = os.environ.get("ODOMETER_CONFIG_PATH", default='config.ini')

    if not os.path.exists(config_file):
        raise FileNotFoundError('Config file "%s" not found. Please check ODOMETER_CONFIG_PATH environment variable' % config_file)

    parser = configparser.ConfigParser()
    parser.read(config_file)

    config['SECRET_KEY'] = parser.get(CONFIG_COMMON, 'secret_key', fallback='')
    config['SAVE_FREQUENCY_SECONDS'] = int(parser.get(CONFIG_COMMON, 'history_save_freq', fallback='15'))
    config['IDLE_SESSION_TIMEOUT_SECONDS'] = int(parser.get(CONFIG_COMMON, 'idle_session_timeout_seconds', fallback=1 * 60))
    config['MINIMUM_SESSION_DURATION_SECONDS'] = int(parser.get(CONFIG_COMMON, 'minimum_session_duration_seconds', fallback=5 * 60))
    config['SERVER_NAME'] = parser.get(CONFIG_COMMON, 'server_name', fallback=None)
    config['APPLICATION_ROOT'] = parser.get(CONFIG_COMMON, 'application_root', fallback=None)
    config['DATABASE_PATH'] = parser.get(CONFIG_COMMON, 'database_path', fallback='data/database.db')
    config['THUMBNAIL_PATH'] = parser.get(CONFIG_COMMON, 'thumbnail_path', fallback='data/thumbnails')
    config['DEBUG'] = bool(parser.get(CONFIG_COMMON, 'debug', fallback='False'))

    return config

