#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#

import time
import logging
import io
import hashlib
from flask import Flask, json, g, session, request, send_file, url_for, make_response
from flask_socketio import SocketIO, send, emit, join_room, leave_room
from flask_cors import CORS
from database import Database
from config import load_config

app = Flask(__name__)

load_config(app.config)
socketio = SocketIO(app)

if app.config['DEBUG']:
    CORS(app, resources={r"/api/*": {"origins": "*"}})

# <Snippet>
# From http://flask.pocoo.org/snippets/35/
class ReverseProxied(object):
    '''Wrap the application in this middleware and configure the
    front-end server to add these headers, to let you quietly bind
    this to a URL other than / and to an HTTP scheme that is
    different than what is used locally.

    In nginx:
    location /myprefix {
        proxy_pass http://192.168.0.1:5001;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Scheme $scheme;
        proxy_set_header X-Script-Name /myprefix;
        }

    :param app: the WSGI application
    '''
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '')
        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]

        scheme = environ.get('HTTP_X_SCHEME', '')
        if scheme:
            environ['wsgi.url_scheme'] = scheme
        return self.app(environ, start_response)
#</Snippet>

app.wsgi_app = ReverseProxied(app.wsgi_app)

def _get_database():
    if not hasattr(g, 'connection'):
        g.connection = Database(app.config['DATABASE_PATH'])
    return g.connection

def _get_or_create_device(device_guid):
    db = _get_database()
    device = db.get_device(device_guid)
    if not device:
        device = db.create_device(device_guid)

    return device

@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'connection'):
        g.connection.close()

def _get_user(user_id):
    db = _get_database()
    user = None
    try:
        user = db.get_user(int(user_id))
    except ValueError:
        pass

    if user is None:
        user = db.get_or_create_user(user_id, False)

    return user

def get_session_stats(session_id):
    db = _get_database()
    session = db.get_session_stats(session_id)
    if session is not None:
        session['chart_url'] = _get_chart_url(session)
    return session

def get_sessions_stats(user_id = None, device_id = None):
    db = _get_database()
    sessions = db.get_sessions_stats(user_id, device_id)

    for session in sessions:
        session['chart_url'] = _get_chart_url(session)

    return sessions

def _get_chart_url(session):
    if session['end_time'] is None:
        return None
    return url_for('.get_session_by_id_chart', session_id=session['session_id'], _external=True)

@app.route('/')
def hello_world():
    return 'OK'

@app.route('/api/v1.0/users', methods=['GET'])
def get_users():
    db = _get_database()
    return json.jsonify(db.get_users())

@app.route('/api/v1.0/users/<user_id>', methods=['GET'])
def get_user(user_id):
    db = _get_database()

    user = _get_user(user_id)
    if user is None:
        return json.jsonify(error=404, text='User not found'), 404

    return json.jsonify(user)

@app.route('/api/v1.0/devices', methods=['GET'])
def get_devices():
    db = _get_database()
    return json.jsonify(db.get_devices())

@app.route('/api/v1.0/devices/<device_id>', methods=['GET'])
def get_device(device_id):
    db = _get_database()
    device = db.get_device_by_id(device_id)
    if device is None:
        return json.jsonify(error=404, text='Device with id %i not found' % device_id), 404
    return json.jsonify(device)

@app.route('/api/v1.0/devices/<device_id>/sessions', methods=['GET'])
def get_device_sessions(device_id):
    db = _get_database()

    device = db.get_device_by_id(device_id)
    if device is None:
        return json.jsonify(error=404, text='Device with id %i not found' % device_id), 404

    # TODO: only do this once every X minutes / once a day or with Celery as a background job!
    db.cleanup_sessions(time.time() - app.config['IDLE_SESSION_TIMEOUT_SECONDS'], app.config['MINIMUM_SESSION_DURATION_SECONDS'])
    return json.jsonify(get_sessions_stats(None, device_id))

@app.route('/api/v1.0/sessions', methods=['GET'])
def get_all_sessions():
    db = _get_database()
    # TODO: only do this once every X minutes / once a day or with Celery as a background job!
    db.cleanup_sessions(time.time() - app.config['IDLE_SESSION_TIMEOUT_SECONDS'], app.config['MINIMUM_SESSION_DURATION_SECONDS'])
    return json.jsonify(get_sessions_stats())

@app.route('/api/v1.0/sessions/<session_id>', methods=['GET'])
def get_session_by_id(session_id):
    db = _get_database()
    odometer_session = get_session_stats(session_id)
    if odometer_session is None:
        return json.jsonify(error=404, text='Odometer session not found'), 404

    return json.jsonify(odometer_session)

@app.route('/api/v1.0/sessions/<session_id>/chart', methods=['GET'])
def get_session_by_id_chart(session_id):
    db = _get_database()
    session_id = int(session_id)
    session = db.get_session(session_id)
    if not session:
        return json.jsonify(error=404, text='Odometer session not found'), 404

    # don't return a chart for session in progress if no end_time is set
    if not session['end_time']:
        return json.jsonify(error=404, text='Odometer session not finished yet, chart not available'), 404

    etag = hashlib.md5(('%i_%i' % (session['session_id'], session['end_time'])).encode('utf-8')).hexdigest()
    if request.if_none_match and etag in request.if_none_match:
        return '', 304

    from chart import ChartManager
    chart_manager = ChartManager(app.config['THUMBNAIL_PATH'])

    path = chart_manager.make_chart(session_id, db)

    response = make_response(send_file(
        path,
        mimetype='image/png'
    ))
    response.set_etag(etag)

    return response

@app.route('/api/v1.0/sessions/<session_id>', methods=['DELETE'])
def delete_session_by_id(session_id):
    db = _get_database()
    session_id = int(session_id)
    if not db.session_exists(session_id):
        return json.jsonify(error=404, text='Odometer session not found'), 404

    db.delete_session(session_id)

    from chart import ChartManager
    chart_manager = ChartManager(app.config['THUMBNAIL_PATH'])
    chart_manager.delete_chart(session_id)

    return ('', 204)

@app.route('/api/v1.0/sessions/<session_id>', methods=['PATCH'])
def update_session_by_id(session_id):
    db = _get_database()
    if not db.session_exists(session_id):
        return json.jsonify(error=404, text='Odometer session not found'), 404

    if not request.is_json:
        return json.jsonify(error=400, text='Missing start_time JSON attribute'), 400

    data = request.json

    keys = data.keys()
    if len(keys) > 1:
        return json.jsonify(error=422, text='PATCH on odometer sessions only supports one attribute "start_time"'), 422

    if not 'start_time' in data:
        return json.jsonify(error=422, text='PATCH on odometer sessions only supports one attribute "start_time"'), 422

    try:
        if int(data['start_time']) <= 0:
            return json.jsonify(error=400, text='Invalid start_time value, must be bigger than zero'), 400
    except ValueError:
        return json.jsonify(error=400, text='Invalid start_time value, must be int'), 400

    if not db.set_session_start(session_id, data['start_time']):
        return json.jsonify(error=500, text='Error during odometer session update'), 500

    odometer_session = get_session_stats(session_id)
    if odometer_session is None:
        return json.jsonify(error=404, text='Odometer session not found'), 404

    return json.jsonify(odometer_session)

@app.route('/api/v1.0/sessions/<session_id>/history', methods=['GET'])
def get_session_history(session_id):
    db = _get_database()
    if not db.session_exists(session_id):
        return json.jsonify(error=404, text='Odometer session not found'), 404

    return json.jsonify(db.get_session_history(session_id))

@app.route('/api/v1.0/users/<user_id>/sessions', methods=['GET'])
def get_sessions_by_user(user_id):
    db = _get_database()

    user = _get_user(user_id)
    if user is None:
        return json.jsonify(error=404, text='User not found'), 404

    return json.jsonify(get_sessions_stats(user['user_id']))

@app.route('/api/v1.0/currentsession', methods=['GET'])
def get_current_session():
    user_id = None # FIXME
    db = _get_database()
    r = db.get_current_device_user_session_by_user(user_id)
    if r is None:
        return json.jsonify(error=404, text='No current session for user %s' % user_id), 404
    return json.jsonify(get_session_stats(r['session_id']))

@app.route('/api/v1.0/currentsession/history', methods=['GET'])
def get_current_session_history():
    user_id = None # FIXME
    db = _get_database()
    r = db.get_current_device_user_session_by_user(user_id)
    if r is None:
        return json.jsonify(error=404, text='No current session for user %s' % user_id), 404
    return json.jsonify(db.get_session_history(r['session_id']))

@app.route('/api/v1.0/devices/<device_id>/currentsession', methods=['GET'])
def get_current_device_session(device_id):
    db = _get_database()
    device = db.get_device_by_id(device_id)
    if device is None:
        return json.jsonify(error=404, text='Device with id %i not found' % device_id), 404
    r = db.get_current_device_user_session_by_device(device_id)
    if r is None:
        return json.jsonify(error=404, text='No current session for device %s' % device_id), 404
    # TODO: perms check on user id
    return json.jsonify(get_session_stats(r['session_id']))

@app.route('/api/v1.0/devices/<device_id>/currentsession/history', methods=['GET'])
def get_current_device_session_history(device_id):
    db = _get_database()
    device = db.get_device_by_id(device_id)
    if device is None:
        return json.jsonify(error=404, text='Device with id %i not found' % device_id), 404
    r = db.get_current_device_user_session_by_device(device_id)
    if r is None:
        return json.jsonify(error=404, text='No current session for device %s' % device_id), 404
    # TODO: perms check on user id
    return json.jsonify(db.get_session_history(r['session_id']))

@socketio.on('json')
def receive_json(json):
    # TODO: commands to control the session/server
    logging.debug('received json: ' + str(json))

def _create_session(device_guid, start_time):
    assert(start_time is not None)
    assert(device_guid is not None)

    db = _get_database()
    device = _get_or_create_device(device_guid)

    logging.debug('Starting session, time %2.3f', start_time)
    session_id = db.start_session(start_time, device['device_id'])
    session['odometer_session_id'] = session_id
    logging.debug('Started session, time %2.3f, id %i', start_time, session['odometer_session_id'])

    emit('session_start', get_session_stats(session_id), room='device-' + device_guid, broadcast=True)

    return session_id

def _get_or_create_odometer_session(device_guid, start_time):
    try:
        session_id = session['odometer_session_id']
        # check that session exists
        db = _get_database()
        if db.session_exists(session_id):
            return session_id
        logging.debug('Session with id %i not found in DB, creating new one', session_id)
    except KeyError:
        logging.debug('No odometer session in session')
        pass

    # retroactively start session
    session_id = _create_session(device_guid, start_time)

    return session_id

def _db_save_history(values):
    # TODO: actually save to DB
    db = _get_database()

    session_id = _get_or_create_odometer_session(values['device_guid'], time.time() - values['elapsed'])
    logging.debug('Saving history, session id %s' % session_id)

    history_id = db.insert_history(
        session_id,
        values['lastTick'],
        values['rotations'],
        values['distance'],
        values['speedRpm'],
        0.0,
        values['heartRate']
    )

    return (history_id, session_id)

def _save_history(values):
    frequency = app.config['SAVE_FREQUENCY_SECONDS']
    timestamp = time.time()
    first = False
    try:
        last_timestamp = session['last_history_timestamp']
    except KeyError:
        first = True
        last_timestamp = timestamp
        session['last_history_timestamp'] = timestamp

    if first or timestamp > last_timestamp + frequency:
        (history_id, session_id) = _db_save_history(values)
        # FIXME: bring some consistency in key names
        history_entry = {
            'session_id': session_id,
            'history_id': history_id,
            'timestamp': values['lastTick'],
            'rotations': values['rotations'],
            'speed': values['speedRpm'],
            'distance': values['distance'],
            'heart_rate': values['heartRate'],
        }
        emit('history', history_entry, room='device-' + values['device_guid'], broadcast=True)
        session['last_history_timestamp'] = timestamp

@socketio.on('start_monitor_device')
def start_monitor_device(device_guid):
    db = _get_database()
    device = db.get_device(device_guid)
    if device is None:
        # TODO: don't fallback to single device
        devices = db.get_devices()
        if len(devices) > 0:
            device = devices[-1]

    if device is None:
        # emit to single client
        emit('request_error', {'status': 404, 'messsage': 'Unknown device id'})

    logging.debug('client joined room: ' + 'device-' + device['device_guid'])
    join_room('device-' + device['device_guid'])

    current_session = None
    r = db.get_current_device_user_session_by_device(device['device_id'])
    if r is not None and r['session_id'] is not None:
        current_session = get_session_stats(r['session_id'])

    emit('session_current', current_session, room='device-' + device['device_guid'], broadcast=False)

    # TODO: send last values

@socketio.on('stop_monitor_device')
def stop_monitor_device(device_guid):
    db = _get_database()
    device = db.get_device(device_guid)
    if device is None:
        # TODO: don't fallback to single device
        devices = db.get_devices()
        if len(devices) > 0:
            device = devices[-1]

    if device is None:
        # emit to single client
        emit('request_error', {'status': 404, 'messsage': 'Unknown device id'})

    logging.debug('client left room: ' + 'device-' + device['device_guid'])
    leave_room('device-' + device['device_guid'])
    # TODO: send last values

@socketio.on('odometer_values')
def receive_values(values):
    # TODO: prevent non-odometer to send values here (shared secret ?)
    logging.debug('received values: ' + str(values))
    # emit to device room
    emit('monitor', values, room='device-' + values['device_guid'], broadcast=True)
    _save_history(values)

@socketio.on('odometer_session_start')
def start_session(values):
    db = _get_database()

    session_id = _create_session(values['device_guid'], values['start_time'])

@socketio.on('odometer_session_end')
def end_session(values):
    db = _get_database()
    try:
        session_id = session['odometer_session_id']
    except KeyError:
        return;

    logging.debug('Ending session, time %2.3f', values['end_time'])

    device = None
    r = db.get_current_device_user_session_by_session(session_id)
    if r is not None:
        device = db.get_device_by_id(r['device_id'])

    db.end_session(session_id, values['end_time'])

    if device is not None:
        emit('session_end', get_session_stats(session_id), room='device-' + device['device_guid'], broadcast=True)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    # TODO: read config
    logging.info('Starting web server')
    socketio.run(app)

    logging.info('Quitting')

